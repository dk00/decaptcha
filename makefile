CC=gcc
CXX=g++
LDLIBS=-llept -ltesseract
TOOLS_SRC=tools.cpp filters.cpp

tools.so: $(TOOLS_SRC)
	$(CXX) -o $@ $(TOOLS_SRC) -fPIC -shared $(LDLIBS)

%.so: %.cpp
	$(CXX) -o $@ $? -fPIC -shared $(LDLIBS)
