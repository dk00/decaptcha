from os.path import dirname, realpath
from ctypes import cdll, c_char_p
tools = cdll.LoadLibrary(dirname(realpath(__file__))+'/tools.so')

def decaptcha(image, lang, whitelist_char):
  tools.init(lang.encode())
  tools.getText.restype = c_char_p
  text = tools.getText(image, whitelist_char.encode()).decode().strip()
  return text

def main():
  sample_chars = 'bcdefghkmnprwxy2345678'
  text_color = 0xffffffff
  back_color = 0x000000ff
  text_color = 0x000000ff
  back_color = 0xffffffff
  import argparse
  ap = argparse.ArgumentParser()
  ap.add_argument('-f', dest='filters', action='append', default=[])
  ap.add_argument('-l', dest='lang', default='eng')
  ap.add_argument('-w', dest='whitelist-char', default=sample_chars)
  ap.add_argument('-b', dest='makebox', action='count')
  ap.add_argument('files', nargs='+')
  args = vars(ap.parse_args())
  
  for name in args['files']:
    image = tools.loadImage(name.encode())
    for func in args['filters']:
      try:
        tools.__getattr__(func)(image, text_color, back_color)
      except:
        print('"%s" is not implemented' % action)
    text = decaptcha(image, args['lang'], args['whitelist-char'])
    print(name, text)
    if args['makebox']:
      basename = '.'.join(name.split('.')[:-1])
      tools.makeBox(image, args['whitelist-char'].encode(), (basename+'.box').encode())
      tools.saveImage((basename+'.png').encode(), image)    

if __name__ == '__main__':
  main()
