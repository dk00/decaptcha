
def main():
  sample_chars = 'bcdefghkmnprwxy2345678'
  text_color = 0xffffffff
  back_color = 0x000000ff
  import os
  import argparse
  ap = argparse.ArgumentParser()
  ap.add_argument('-l', dest='lang', default='eng')
  ap.add_argument('-w', dest='whitelist-char', default=sample_chars)
  ap.add_argument('files', nargs='+')
  args = vars(ap.parse_args())
  
  boxes = []
  trained = []
  commands = []
  for name in args['files']:
    basename = '.'.join(name.split('.')[:-1])
    command = 'echo %s >> train.log;tesseract 2>&1 %s eng.sample0.%s -l sample0 -psm 8 nobatch box.train | tee -a train.log' % (name, name, basename)
    print(command)
    os.system(command)
    trained_name = 'eng.sample0.%s.tr' % basename
    if os.path.exists(trained_name):
      trained.append(trained_name)
      boxes.append(basename+'.box')
  trained = ' '.join(trained)
  commands.append('unicharset_extractor ' + ' '.join(boxes))
  commands.append('shapeclustering -F font_properties -U unicharset ' + trained)
  commands.append('mftraining -F font_properties -U unicharset ' + trained)
  commands.append('cntraining ' + trained)
  for command in commands:
    print(command)
    os.system(command)
  names = ['unicharset', 'normproto', 'pffmtable', 'shapetable', 'inttemp']
  for name in names:
    print(name, 'eng.'+name)
    os.rename(name, 'eng.'+name)

if __name__ == '__main__':
  main()
