#define DLLEXP extern "C"
#include <leptonica/allheaders.h>
#define DEBUG fprintf
const unsigned kTextColor = 0xffffffff, kBackColor = 0x000000ff;
DLLEXP void binary(Pix *image, unsigned text_color = kTextColor, unsigned back_color = kBackColor) {
  unsigned i, luma, luma_2nd, n;
  l_int32 w, h;
  l_uint32 *pixels = pixGetData(image);
  pixGetDimensions(image, &w, &h, NULL);
  n = w*h;
  unsigned *count = new unsigned[1<<16];
  for (luma = 0; luma < (1<<16); luma++)
    count[luma] = 0;
  for (i = 0; i < n; i++) {
    luma = pixels[i];
    luma = pixels[i] = 
      (luma>>28) + ((luma>>20)&0xf) + ((luma>>12)&0xf) + ((luma>>4)&0xf);
    count[luma]++;
  }
  for (i = luma = 0; i < (1<<16); i++)
    if (count[i] > count[luma])
      luma = i;
  luma_2nd = luma^0xffff;
  for (i = 0; i < (1<<16); i++)
    if (i != luma && count[i] > count[luma_2nd])
      luma_2nd = i;
  if (count[luma]*2 > n) luma = luma_2nd;
  for (i = 0; i < n; i++) {
    if (pixels[i] == luma)
      pixels[i] = text_color;
    else
      pixels[i] = back_color;
  }
}

DLLEXP void separate(Pix *image, unsigned text_color = kTextColor, unsigned back_color = kBackColor) {
  const int kEdgeMin = 10, kEdgeMax = 19, kEdgeLim = 23;
  unsigned i, j, n, p, conn, edge_top, edge_bot;
  unsigned prev_conn, prev_top, prev_bot, prev_clear;
  l_int32 w, h;
  l_uint32 *pixels = pixGetData(image);
  pixGetDimensions(image, &w, &h, NULL);
  n = w*h;
  unsigned *clear = new unsigned[w], *line_len = new unsigned[w], 
           *top = new unsigned[w], *bot = new unsigned[w];
  for (j = 0; j < w; j++) {
    clear[j] = 0;
    line_len[j] = 0;
    bot[j] = 0;
    top[j] = h;
  }
  for (j = 0; j < w; j++) {
    for (i = conn = 0; i < h; i++){
      p = pixels[i*w+j];
      if (p == text_color) {
        if (top[j] > i)
          top[j] = i;
        bot[j] = i;
        conn++;
        continue;
      }
      if (conn > line_len[j])
        line_len[j] = conn;
      conn = 0;
    }
  }
  conn = edge_top = edge_bot = prev_clear = 0;
  for (j = 1; j < w; j++) {
    prev_conn = conn;
    prev_top = edge_top;
    prev_bot = edge_bot;
    conn = edge_top = edge_bot = 0;
    if (line_len[j] > kEdgeMin) {
      conn = prev_conn+1;
      if (abs(top[j-prev_top-1] - top[j]) < 2)
        edge_top = prev_top+1;
      if (abs(bot[j-prev_bot-1] - bot[j]) < 2)
        edge_bot = prev_bot+1;
    }
    if (edge_top == 0 && prev_top > 3) {
      p = j - prev_top - 1;
      int count;
      for (i = count = 0; i < h; i++)
        if (pixels[i*w+p-1] == text_color)
          count++;
      if (count == line_len[p-1] && count < kEdgeLim && 
          (j-prev_clear > 13 || j-prev_clear < 10)) {
        clear[p] = 1;
        prev_conn = conn = 0;
        prev_clear = j;
      }
    }
    if (edge_bot == 0 && prev_bot > 4 && edge_top < prev_bot) {
      p = j-1;
      if (prev_bot > 8) p = j-6;
      if (line_len[p+1] > kEdgeMin && 
          (j-prev_clear > 13 || j-prev_clear < 10)) {
        clear[p] = 1;
        prev_conn = conn = prev_top = edge_top = 0;
        prev_clear = j;
      }
    }
    if (conn == 0 && prev_conn > 10) {
      clear[j-prev_conn/2-1] = 1;
    }
  }
  edge_top = 0;
  for (j = 1; j < w; j++) {
    if (abs(top[j-1-edge_top] - top[j]) < 2) {
      edge_top++;
      continue;
    }
    if (edge_top > 30)
      clear[j-edge_top/2-1] = 1;
    edge_top = 0;
  }
  edge_bot = 0;
  for (j = 1; j < w; j++) {
    if (abs(bot[j-1-edge_bot] - bot[j]) < 2) {
      edge_bot++;
      continue;
    }
    if (edge_bot > 22)
      clear[j-7] = 1;
    edge_bot = 0;
  }
  for (j = 0; j < w; j++) {
    DEBUG(stderr, "%d ", clear[j]);
  }
  DEBUG(stderr, "\n",);
  for (j = 0; j < w; j++) {
    if (!clear[j]) continue;
    line_len[j] = 0;
    for (i = 0; i < h; i++)
      pixels[i*w+j] = back_color;
  }
  delete[] line_len, top, bot;
}

#include<queue>
DLLEXP void fill(Pix *image, unsigned text_color = kTextColor, unsigned back_color = kBackColor) {
  l_int32 w, h;
  l_uint32 *pixels = pixGetData(image);
  pixGetDimensions(image, &w, &h, NULL);
  unsigned i, j, n;
  n = w*h;
  l_uint32 *ori = new l_uint32[n];
  bool *filled = new bool[n];
  for (i = 0; i < n; i++) {
    if (pixels[i] > 0x10)
      ori[i] = text_color;
    else
      ori[i] = back_color;
    pixels[i] = back_color;
    filled[i] = false;
  }
  const int d[] = {w, 1, -w, -1};
  std::queue<int> q, q1;
  q.push(0);
  while (!q.empty()) {
    i = q.front();
    q.pop();
    for (j = 0; j < 4; j++) {
      if (i+d[j] < 0 || i+d[j] > n || filled[i+d[j]])
        continue;
      filled[i+d[j]] = true;
      if (ori[i+d[j]] == text_color)
        q1.push(i+d[j]);
      else
        q.push(i+d[j]);
    }
  }
  while (!q1.empty()) {
    i = q1.front();
    q1.pop();
    if (ori[i] == back_color)
      pixels[i] = text_color;
    for (j = 0; j < 4; j++)
      if (0 <= i+d[j] && i+d[j] < n && 
          !filled[i+d[j]] && ori[i+d[j]] == back_color) {
        filled[i+d[j]] = true;
        q1.push(i+d[j]);
      }
  }
  delete[] filled, ori;
}

int side_score(int n, int w, int x, int y, int d0, int d1, const unsigned *sum) {
  int i, j, side0 = 0, side1 = 0;
  for (i = -1; i <= 1; i++) {
    j = x*w+y + i*d0 + d1;
    if (j > 0 && j < n && sum[j] > side0)
      side0 = sum[j];
    j = x*w+y + i*d0 - d1;
    if (j > 0 && j < n && sum[j] > side1)
      side1 = sum[j];
  }
  if (side0 < 2 || side1 < 2)
    return 0;
  if (side0 < 3 || side1 < 3)
    return 1;
  return 2;
}

DLLEXP void denoise(Pix *image, unsigned text_color = kTextColor, unsigned back_color = kBackColor) {
  l_int32 w, h;
  l_uint32 *pixels = pixGetData(image);
  pixGetDimensions(image, &w, &h, NULL);
  int i, j, n;
  n = w*h;
  unsigned *vsum = new unsigned[n], *hsum = new unsigned[n];
  for (i = 0; i < h; i++)
    for (j = 0; j < w; j++) {
      hsum[i*w+j] = vsum[i*w+j] = 0;
      if (pixels[i*w+j] != text_color)
        continue;
      hsum[i*w+j] = vsum[i*w+j] = 1;
      if (i > 0)
        hsum[i*w+j] += hsum[(i-1)*w+j];
      if (j > 0)
        vsum[i*w+j] += vsum[i*w+j-1];
    }
  for (i = h-1; i >= 0; i--)
    for (j = w-1; j >= 0; j--) {
      if (i > 0 && hsum[(i-1)*w+j] && hsum[i*w+j])
        hsum[(i-1)*w+j] = hsum[i*w+j];
      if (j > 0 && vsum[i*w+j-1] && vsum[i*w+j])
        vsum[i*w+j-1] = vsum[i*w+j];
    }
  for (i = 0; i < h; i++) {
    for (j = 0; j < w; j++) {
      if (hsum[i*w+j]+side_score(n, w, i, j, w, 1, hsum) < 4 ||
          vsum[i*w+j]+side_score(n, w, i, j, 1, w, vsum) < 4)
        hsum[i*w+j] = vsum[i*w+j] = 0;
    }
  }
  for (i = 0; i < n; i++)
    if (hsum[i] > 0 && vsum[i] > 0)
      pixels[i] = text_color;
    else
      pixels[i] = back_color;
  delete[] vsum, hsum;
}
