#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>

#define LOWER "abcdefghijklmnopqrstuvwxyz"
#define UPPER "ABCDEFGHIJKLMNOPQRSTYVWXYZ"
#define NUMBER "0123456789"

void conv_binary(Pix *image) {
  unsigned i, p, n;
  l_int32 w, h;
  l_uint32 *pixels = pixGetData(image);
  pixGetDimensions(image, &w, &h, NULL);
  n = w*h;
  unsigned *count = new unsigned[65536];
  for (p = 0; p < 65536; p++)
    count[p] = 0;
  for (i = 0; i < n; i++) {
    p = pixels[i];
    pixel[i] = (p>>28) + ((p>>20)&0xf) + ((p>>12)&0xf) + ((p>>4)&0xf);
    count[p]++;		
  }
  for (i = p = 0; i < 65536; i++)
    if (count[p] > count[i])
      i = p;
  for (i = 0; i < n; i++) {
    pixels[i] = 0xff;
    if (pixels[i] != p)
      pixels[i] = 0xffffffff;
  }
}

void separate(Pix *image) {
  int hmin = 10, hmax = 19, hlim = 23;
  int i, j, n, luma, cont_black, cont_line, cont_equal;
  unsigned p;
  l_int32 w, h;
  l_uint32 *pixels = pixGetData(image);
  pixGetDimensions(image, &w, &h, NULL);
  n = w*h;
  int *height = new int[w];
  for (j = 0; j < w; j++)
    height[j] = 0;
  for (j = 0; j < w; j++) {
    for (i = cont_black = 0; i < h; i++){
      p = pixels[i*w+j];
      luma = (p>>24) + ((p>>16)&0xff) + ((p>>8)&0xff) + (0xff-(p&0xff));
      printf("%d ", luma);
      if (luma < 16) {
        cont_black++;
        continue;
      }
      if (cont_black > 0)
        printf("%d ", cont_black);
      if (cont_black > height[j])
        height[j] = cont_black;
      cont_black = 0;
    }
    printf("%d ", height[j]);
  }
  puts("");
  cont_line = cont_equal = 0;
  for (j = 1; j < w; j++) {
    if (height[j] > hmax && abs(height[j-1] - height[j]) < 1)
      cont_equal++;
    else {
      if (cont_equal > 2)
        printf("%d %d\n", cont_equal, height[j-cont_equal-2]);

      if (cont_equal > 2 && height[j-cont_equal-2] < hlim)
        for (i = 0; i < h; i++)
          pixels[i*w+j-cont_equal-1] = 0;
      cont_equal = 0;
    }
    if (height[j] > hmin)
      cont_line++;
    else {
      if (cont_line > 0)
        printf("%d\n", cont_line);
      if (cont_line > 8) {
        for (i = p = j-cont_line*3/4; i < j; i++)
          if (height[i] < height[p])
            p = i;
        for (i = 0; i < h; i++)
          pixels[i*w+p] = 0;
      }
      cont_line = 0;
    }
  }
  delete[] height;
}

int main(int argc, char **argv) {
  char *outText;
  tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
  if (api->Init(NULL, "sample0")) {
    fprintf(stderr, "Could not initialize tesseract.\n");
    exit(1);
  }

  Pix *image = pixRead(argv[1]);
  separate(image);
  pixWrite(argv[2], image, IFF_PNG);

  return 0;
  api->SetImage(image);
  api->SetVariable("tessedit_char_whitelist", NUMBER LOWER);
  outText = api->GetUTF8Text();
  printf("%s", outText);

  api->End();
  delete [] outText;
  pixDestroy(&image);
  return 0;
}
