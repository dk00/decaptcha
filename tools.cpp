#define DLLEXP extern "C"
#include <leptonica/allheaders.h>
#include <tesseract/baseapi.h>
DLLEXP Pix *loadImage(const char *filename) {
  Pix *image = pixRead(filename);
  return image;
}
DLLEXP int saveImage(const char *filename, Pix *image, int format=IFF_PNG) {
  return pixWrite(filename, image, IFF_PNG);
}
DLLEXP void freeImage(Pix *image) {
  pixDestroy(&image);
}

tesseract::TessBaseAPI *tess_api;
DLLEXP int init(const char *lang) {
  tess_api = new tesseract::TessBaseAPI();
  if (tess_api->Init(NULL, lang)) {
    fprintf(stderr, "Could not initialize tesseract.\n");
    return -1;
  }
  return 0;
}

DLLEXP void makeBox(Pix *image, const char *char_whitelist, const char *fileout) {
  tess_api->SetImage(image);
  tess_api->SetVariable("tessedit_char_whitelist", char_whitelist);
  tess_api->SetPageSegMode(tesseract::PSM_SINGLE_WORD);
  tess_api->Recognize(NULL);
  tesseract::ResultIterator* ri = tess_api->GetIterator();
  tesseract::PageIteratorLevel level = tesseract::RIL_SYMBOL;
  FILE *fo = fopen(fileout, "w");
  while (ri) {
    const char* symbol = ri->GetUTF8Text(level);
    //float conf = ri->Confidence(level);
    if(symbol != 0) {
      int x1, y1, x2, y2;
      ri->BoundingBox(level, &x1, &y1, &x2, &y2);
      fprintf(fo, "%s %d %d %d %d 0\n", symbol, x1, 50-y2, x2, 50-y1);
    }
    delete[] symbol;
    if (!ri->Next(level)) break;
  }
  fclose(fo);
}
DLLEXP const char *getText(Pix *image, const char *char_whitelist) {
  tess_api->SetImage(image);
  tess_api->SetVariable("tessedit_char_whitelist", char_whitelist);
  tess_api->SetPageSegMode(tesseract::PSM_SINGLE_WORD);
  const char *text = tess_api->GetUTF8Text();
  return text;
}
